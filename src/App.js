import React from 'react';
import logo from './logo.svg';
import './App.css';
import Conversor from "./components/Conversor";

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <div>
          <Conversor/>
        </div>

      </header>
    </div>
  );
}

export default App;
