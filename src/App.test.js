//import toDigital from "./funciones/Numero.js"

const { toDigital } = require("./funciones/Numero.js")


describe("Mi primer test", () => {
  it("Espero que retorne true", () => {
    expect(true);
  });
});

describe("test de nros", () => {
  it("Espero que retorne los valore para armar un nro digital", () => {
   expect(toDigital(0)).toEqual([ ' _ ', '| |', '|_|' ]);
   expect(toDigital(1)).toEqual([ '  ', ' |', ' |' ]);
   expect(toDigital(5)).not.toEqual([ '  ', 'fdafa', ' |' ]);
   expect(toDigital(9)).not.toEqual([ 'fsa455', '| |', ' |' ]);
   expect(toDigital(7)).toEqual([ ' _ ', '  |', '  |' ]);
   expect(toDigital(8)).toEqual([ ' _ ', '|_|', '|_|' ]);
  });
});
