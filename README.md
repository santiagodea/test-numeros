#### Actividad III: TDD+CI
Desarrollo una librería que convierta cualquier número (de cualquier cantidad de dígitos) al formato:

>   _  _     _  _  _  _  _  _
> | _| _||_||_ |_   ||_||_|| |
> ||_  _|  | _||_|  ||_| _||_|
>

Siempre siguiendo la práctica de TDD.



La solución a entregar debe ser un repositorio en Gitlab que provea la funcionalidad indicada, y que los tests se corran ante cada commit

## Available Scripts

In the project directory, you can run:

### `npm start`

Open [http://localhost:3000](http://localhost:3000) to view it in the browser.




### `npm test`

Santiago De Andrea

esta configurado Pipelines para Gitlab CI

### [Actividad 1](https://gitlab.com/santiagodea/test-numeros/blob/master/ActividadI.md)


