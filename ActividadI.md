# Actividad 1: estándares de codificación
> Investigar qué lenguajes son los más utilizados (al menos 3) para desarrollo web,
> así como librerías y/o frameworks.
>Luego, para cada uno determinar cuál es el estándar de codificación propuesto.

#### Ranking de lenguajes mas usados, según distintos informes:
##### Según: [techrepublic.com](https://www.techrepublic.com/article/forget-the-most-popular-programming-languages-heres-what-developers-actually-use/)
 - Python
 - Ruby
 - JavaScript
 - Java
 - C++

##### Según: [towardsdatascience.com](https://towardsdatascience.com/visualize-programming-language-popularity-using-tiobeindexpy-f82c5a96400d)
 - Java
 - C
 - Python
 - C++
 - .NET

##### Según: [houseofbots.com](https://www.houseofbots.com/news-detail/4504-1-top-20-most-popular-programming-languages-to-learn-in-2019)
 - Python
 - Java
 - JavaScript
 - C#
 - PHP

##### Según: [tiobe.com](https://www.tiobe.com/tiobe-index/)
 - Java
 - C
 - Python
 - C++
 - C#

##### según: [businessinsider.com](https://www.businessinsider.com/the-10-most-popular-programming-languages-according-to-github-2018-10#6-c-5)
 - JavaScript
 - Java
 - Python
 - PHP
 - C++

##### Segun: [hackernoon.com](https://hackernoon.com/8-top-programming-languages-frameworks-of-2019-2f08d2d21a1)
 - JavaScript
 - Python
 - Java
 - Php
 - SQL
 - Ruby

Como conclusión puedo decir que los lenguajes mas utilizados depende en parte de que es lo que queremos hacer.
Por ejemplo en la programación **Frontend** los mas utilizados son: **JavaScript** con **CSS + HTML** y algunos de los Frameworks y librerias mas usadas son:
**React**, **Redux**, **Angular**, **Bootstrap**, **Vue**, **Foundation**, **LESS**, **Sass**, **Stylus** y **PostCSS**.

Pero si hablamos de hacer un **Backend** los mas utilizados en este caso son: **Python, **PHP**, **Ruby**, **C#** y **Java**.
En este caso algunos de los Frameworks mas usados son: **Django**, **Laravel**, **Ruby On Rails** y **ASP.Net**.

#### Principales estandares de codificacion:
 - [Java](https://www.oracle.com/technetwork/java/codeconventions-150003.pdf)
 - [JavaScript](https://standardjs.com/)
 - [PHP](https://www.php-fig.org/psr/psr-1/)
 - [Ruby](https://shopify.github.io/ruby-style-guide/)